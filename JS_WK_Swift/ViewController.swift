//
//  ViewController.swift
//  JS_WK_Swift
//
//  Created by BlackChen on 2020/12/30.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate, WKScriptMessageHandler {
    // 加载html
        let html = try! String(contentsOfFile: Bundle.main.path(forResource: "index", ofType: "html")!, encoding: String.Encoding.utf8)

    
        // 加载完毕以后执行
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            //TODO: 调用JS方法
            webView.evaluateJavaScript("sayHello('WebView你好！')") { (result, err) in
                // result是JS返回的值
                print(result, err)
            }
        }

        // Swift方法，可以在JS中调用
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            print(message.body)
        }
    
//    注入脚本
    // 创建WKUserContentController
//    let userContentController = WKUserContentController()
//    // 加载WKUserScript
//    let userScript = .loadUserScript(with: "html/local_script")
//    // 注入WKUserScript
//    userContentController.addUserScript(userScript)
//
//    // addUserScript(_:) 接收一个WKUserScript对象
//    func loadUserScript(with path: String) -> WKUserScript {
//        // 加载本地js文件
//        let filePath = Bundle.main.path(forResource: path, ofType: "js")
//        // js文件内容
//        var script:String?
//
//        do {
//            script = try String(contentsOfFile: filePath!, encoding: String.Encoding.utf8)
//        }
//        catch {
//            print("Cannot Load File")
//        }
//
//        return WKUserScript(source: script!, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
//    }
//
////    防止内存泄漏
//    deinit {
//        userContentController.removeScriptMessageHandler(forName: "callbackHandler")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        title = "WebView与JS交互"
        view.addSubview(webView)

        webView.loadHTMLString(html, baseURL: nil)
    }

    lazy var webView: WKWebView = {
        // 创建WKPreferences
        let preferences = WKPreferences()
        // 开启js
        preferences.javaScriptEnabled = true
        // 创建WKWebViewConfiguration
        let configuration = WKWebViewConfiguration()
        // 设置WKWebViewConfiguration的WKPreferences
        configuration.preferences = preferences
        // 创建WKUserContentController
        let userContentController = WKUserContentController()
        // 配置WKWebViewConfiguration的WKUserContentController
        configuration.userContentController = userContentController
        // 给WKWebView与Swift交互起一个名字：callbackHandler，WKWebView给Swift发消息的时候会用到
        // 此句要求实现WKScriptMessageHandler协议
        configuration.userContentController.add(self, name: "callbackHandler")

        // 创建WKWebView
        var webView = WKWebView(frame: self.view.frame, configuration: configuration)
        // 让webview翻动有回弹效果
        webView.scrollView.bounces = true
        // 只允许webview上下滚动
        webView.scrollView.alwaysBounceVertical = true
        // 此句要求实现WKNavigationDelegate协议
        webView.navigationDelegate = self
        
        return webView
    }()

    
}

// 拦截、并处理 alert()
    extension ViewController: WKUIDelegate {
        // 读取window.alert()
        func webView(_ webView: WKWebView,
                     runJavaScriptAlertPanelWithMessage message: String,
                     initiatedByFrame frame: WKFrameInfo,
                     completionHandler: @escaping () -> Void) {
            // 处理
        }
    
//    拦截、并处理 confirm()
        // 读取window.confirm()
        func webView(_ webView: WKWebView,
                     runJavaScriptConfirmPanelWithMessage message: String,
                     initiatedByFrame frame: WKFrameInfo,
                     completionHandler: @escaping (Bool) -> Void) {
            // 处理
        }
//    拦截、并处理 prompt()
        // 1.读取widnow.prompt()
        func webView(_ webView: WKWebView,
                     runJavaScriptTextInputPanelWithPrompt prompt: String,
                     defaultText: String?,
                     initiatedByFrame frame: WKFrameInfo,
                     completionHandler: @escaping (String?) -> Void) {

            //处理
        }
    }

